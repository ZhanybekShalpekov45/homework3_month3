from pprint import pprint

from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from db.orders_table import get_orders, save_orders_results


class Bot_By_Orders(StatesGroup):
    name = State()
    age = State()
    location = State()
    gender = State()


async def start_survey(message: types.Message):
    """Запускает опрос"""
    await Bot_By_Orders.name.set()
    await message.reply("Как вас зовут?")


async def process_name(message: types.Message, state: FSMContext):
    """Обрабатывает сообщение от пользователя о его имени"""
    send_name = message.text
    if not send_name.isalpha():
        await message.answer("Введите ваше имя буквами!")
    elif len(str(send_name)) < 2 or len(str(send_name)) > 34:
        await message.answer("Введите имя не менее 2 букв и не более 34")
    else:
        async with state.proxy() as data:
            data["name"] = str(send_name)
            pprint(data.as_dict())
        await message.answer("Очень приятно познакомиться.")

    await Bot_By_Orders.next()
    await message.reply("Сколько вам лет?")


async def proces_age(message: types.Message, state: FSMContext):
    """Обрабатывает сообщение от пользователя о его возрасте"""
    send_age = message.text
    if not send_age.isdigit():
        await message.answer("Введите ваш возраст ЦИФРАМИ!")
    elif int(send_age) < 16 or int(send_age) > 100:
        await message.answer(
            "Наша компания принимает заказы только с 16 до 100 лет."
        )
        await state.finish()
    else:
        async with state.proxy() as data:
            data["age"] = int(send_age)
            pprint(data.as_dict())
        await message.answer("Вы,соответствуете возрастным требованиям.")

    await Bot_By_Orders.next()
    await message.reply("Где вы находитесь?")


async def process_location(message: types.Message, state: FSMContext):
    """Обрабатывает сообщение от пользователя о его местоположении"""
    send_location = message.text
    if not send_location.isalnum():
        await message.answer(
            "Введите название улицы буквами и номер дома цифрами!"
        )
    else:
        async with state.proxy() as data:
            data["location"] = send_location
            pprint(data.as_dict())
        await Bot_By_Orders.next()
        kb = types.ReplyKeyboardMarkup()
        kb.add("Мужской", "Женский")
        await message.answer("Введите пол:", reply_markup=kb)


async def process_gender(message: types.Message, state: FSMContext):
    """Обрабатывает сообщение от пользователя о поле"""
    async with state.proxy() as data:
        data['gender'] = message.text
        pprint(data.as_dict())
        save_orders_results(data.as_dict())

        await state.finish()
        pprint(data.as_dict())
        await message.answer("Спасибо за заказ он скоро будет доставлен.")


async def orders_all(message: types.Message):
    orders = get_orders()
    pprint(orders)
    id_order = 1
    for i in orders:
        await message.answer(f'{id_order}) {i[0]}  {i[1]} цена: {i[2]} сом')
        id_order += 1


def register_survey_handlers(dp: Dispatcher):
    dp.register_message_handler(start_survey, commands=['surv'])
    dp.register_message_handler(process_name, state=Bot_By_Orders.name)
    dp.register_message_handler(proces_age, state=Bot_By_Orders.age)
    dp.register_message_handler(process_location, state=Bot_By_Orders.location)
    dp.register_message_handler(process_gender, state=Bot_By_Orders.gender)
