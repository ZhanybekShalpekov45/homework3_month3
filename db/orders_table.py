import sqlite3
from pathlib import Path


def init_db():
    """Создания соединения с базой данных sqlite"""
    global db, cursor
    DB_PATH = Path(__file__).parent.parent
    DB_NAME = 'db.sqlite'
    db = sqlite3.connect(DB_PATH / DB_NAME)
    cursor = db.cursor()


def create_tables():
    """Создание таблицы"""
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS orders(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    age INTEGER,
    location TEXT,
    gender TEXT
    )''')
    cursor.execute('''CREATE TABLE IF NOT EXISTS orders(
        price_id INTEGER PRIMARY KEY,
        location TEXT,
        expenses INTEGER,
        income FLOATING
    )''')
    db.commit()


def populate_tables():
    """Названия полей для таблицы"""
    cursor.execute("""
    INSERT INTO orders(name, age, location, gender)
    VALUES ('Maksat',16,'Bishkek','Мужской'),
           ('Kairat',21,'Osh','Мужской'),
           ('Karina',19,'Kara-Balta','Женский')
    """)
    db.commit()


def save_orders_results(data):
    """Сохранение заказов в таблицу"""
    cursor.execute(
        """
        INSERT INTO orders(name, age, location, gender)
        VALUES (:name, :age, :location, :gender)
        """,
        {'name': data['name'],
            'age': data['age'],
            'location': data['location'],
            'gender': data['gender']}
    )
    db.commit()


def drop_tables():
    cursor.execute("""
    DROP TABLE IF EXISTS orders
    """)
    db.commit()


def get_orders():
    orders = cursor.execute("""
    SELECT * FROM orders;
    """)
    return orders.fetchall()


if __name__ == "__main__":
    init_db()
    drop_tables()
    create_tables()
    populate_tables()
