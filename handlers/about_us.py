from aiogram import types

from handlers.constants import ABOUT_US


async def about_us(callback: types.CallbackQuery):
    """Возвращает информацию в текствовом виде пользователю"""
    await callback.message.answer(ABOUT_US)
